using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameEnding gameEnding;

    public HealthBar healthBar;
    public PlayerHealth playerHealth;
    public PlayerHealth takeDamage;
    
    public int currentHealth;
    public int damage;
    
     public CanvasGroup caughtBackgroundImageCanvasGroup;

    static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("GameManager");
                    _instance = container.AddComponent<GameManager>();
                }
            }
            return _instance;
        }
    }

   

    //Nathan said dont allow this to happen every frame, also need a player health indicator, also add a timer capability
    public static void PlayerHit()
    {

        Instance.currentHealth -= 5;
        //Debug.Log(Instance.currentHealth);
        Debug.Log("meggie: player hit 46");

        if(Instance.currentHealth >= 0)
        {
            Debug.Log("Player Dead in game manager module line 42!");

            Instance.playerHealth.TakeDamage(5);
            //Instance.gameEnding.CaughtPlayer();
        }

        if (Instance.currentHealth <= 0)
        {
            Instance.gameEnding.CaughtPlayer();
            
        }

        


    }
}