using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public int damage = 5;
    public int minDamage = 0;
    
   
    public HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        damage = minDamage;
        
    }


    public void TakeDamage(int damage)
    {
        Debug.Log("Player health: 29");

        
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);
    }


        //Update is called once per frame
    //void Update()
   // {
        //if (Input.GetKeyDown(KeyCode.Space))
        //if (PlayerHit = True)
       // {
           // Debug.Log("Player hit in PlayerHealth 36!");
            //TakeDamage(20);
        //}
    //}

//this is in the observer script
    //void OnCollisionEnter(Collision collision)
    //{
       // Debug.Log("Player hit on collision before if!");
       // Enemy other = collision.gameObject.GetComponent<Enemy>();
        //if(other){
            // HERE we know that the other object we collided with is an enemy
           // TakeDamage(20);
       // }
    //}
     
}

