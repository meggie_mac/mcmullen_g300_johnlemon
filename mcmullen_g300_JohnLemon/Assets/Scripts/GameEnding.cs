using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public float timeValue = 90;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    private int count;

    public Text timeText;
    public Text instructions;

    //Part of player health:
    //public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;
    
    void Start()
    {
        count = 0;

    }

    
    void OnTriggerEnter (Collider other)
    {
        
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;

        }
    }

    //void OnTriggerEnter (Collider other)
    //{
        //if (other.gameObject == player)
        //{
            //m_IsPlayerAtExit = true;
        //}
    //}


    // Maybe combine these? I want 3 damage and then caught
    public void CaughtPlayer ()
    {
        Debug.Log("GameEnding 64");
        
        //if (currentHealth <= 0)
        
    
        m_IsPlayerCaught = true;
        
    }

    //key end level trigger
    void Update ()
    {
        if (count >= 1)
        {
            EndLevel (exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel (caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        hasKey();


        if (timeValue > 0)
            {
                timeValue -= Time.deltaTime;
            }
        else
            {
                timeValue = 0;
                EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            }
        DisplayTime(timeValue);

        //if (Input.GetKey(KeyCode.Space))
        //{
            //instructions.SetActive(false);
        //}
    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void hasKey()
    {
        if (count == 1)
        {
            EndLevel (exitBackgroundImageCanvasGroup, false, exitAudio);
        }
    }

    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
            
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene (0);
            }
            else
            {
                Application.Quit ();
            }
        }
    }
}
